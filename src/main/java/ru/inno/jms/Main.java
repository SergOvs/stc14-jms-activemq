package ru.inno.jms;

import ru.inno.jms.request.Item;
import ru.inno.jms.request.ItemRequest;

import javax.jms.JMSException;
import java.util.Scanner;

/**
 * Формирует запрос для получения из базы определенного товара
 * Затем ожидает получения результата
 */
public class Main {
    public static void main(String[] args) throws JMSException, InterruptedException {
        int id = Integer.parseInt(args[0]);

        Sender sender = new Sender(
                "Main.Sender." + id,
                "ItemDAORequest"
        );
        Receiver receiver = new Receiver(
                "Main.Receiver." + id,
                "ItemDAOResponse." + id
        );

        while (true) {
            ItemRequest request = new ItemRequest(id);
            sender.sendRequest(request,
                    String.valueOf(id),
                    receiver.getDestination());

            Thread.sleep(1000);

            Item item = receiver.receiveItem(String.valueOf(id));
            System.out.println(item);
        }
/*
        sender.close();
        receiver.close();
*/
    }
}
