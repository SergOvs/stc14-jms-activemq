package ru.inno.jms;

import com.google.gson.Gson;
import ru.inno.jms.request.Item;
import ru.inno.jms.request.ItemRequest;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.TextMessage;
import java.util.Scanner;

public class DBWorker {
    public static void main(String[] args) throws JMSException {
        Receiver receiver = new Receiver(
                "DBWorker.Receiver.",
                "ItemDAORequest");

        receiver.setListener(message -> {
            Gson gson = new Gson();
            try {
                ItemRequest request = gson.fromJson(
                        ((TextMessage)message).getText(),
                        ItemRequest.class);

                System.out.println(request);
                Item item = new Item(
                        request.getId(),
                        "lala" + request,
                        12.45,
                        ""
                );

                Destination destination = message.getJMSReplyTo();
                if (destination != null) {
                    MessageProducer producer = receiver.getSession().createProducer(destination);
                    TextMessage responseTextMessage = receiver.getSession().createTextMessage();
                    responseTextMessage.setText(gson.toJson(item));
                    producer.send(responseTextMessage);
                }

            } catch (JMSException e) {
                e.printStackTrace();
            }
        });
    }
}
