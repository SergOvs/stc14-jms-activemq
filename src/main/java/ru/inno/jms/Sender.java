package ru.inno.jms;

import com.google.gson.Gson;
import org.apache.activemq.ActiveMQConnectionFactory;
import ru.inno.jms.request.Item;
import ru.inno.jms.request.ItemRequest;

import javax.jms.*;
import java.util.UUID;

public class Sender {
    private ConnectionFactory factory;
    private Connection connection;
    private Session session;
    private Destination destination;
    private MessageProducer producer;

    public Sender(String clientId, String topicName) {
        factory = new ActiveMQConnectionFactory(
                ActiveMQConnectionFactory.DEFAULT_BROKER_URL
        );

        try {
            connection = factory.createConnection();
            connection.setClientID(clientId);
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            destination = session.createQueue(topicName);
            producer = session.createProducer(destination);
            connection.start();
        } catch (JMSException e) {
            // TODO: 25.02.2019 логировать
            e.printStackTrace();
        }
    }

    public void sendRequest(String request) throws JMSException {
        TextMessage message = session.createTextMessage();
        message.setText(request);
        producer.send(message);
    }

    public <T> void sendRequest(T request, String id, Destination destination) throws JMSException {
        TextMessage message = session.createTextMessage();
        Gson gson = new Gson();
        message.setText(gson.toJson(request));
        message.setJMSCorrelationID(id);
        message.setJMSReplyTo(destination);
        producer.send(message);
    }

    public void close() {
        try {
            connection.close();
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
