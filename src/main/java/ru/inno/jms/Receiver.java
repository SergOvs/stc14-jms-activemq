package ru.inno.jms;

import com.google.gson.Gson;
import org.apache.activemq.ActiveMQConnectionFactory;
import ru.inno.jms.request.Item;
import ru.inno.jms.request.ItemRequest;

import javax.jms.*;

public class Receiver {
    private ConnectionFactory factory;
    private Connection connection;
    private Session session;
    private Destination destination;
    private MessageConsumer consumer;

    public Receiver(String clientId, String topicName) {
        factory = new ActiveMQConnectionFactory(
                ActiveMQConnectionFactory.DEFAULT_BROKER_URL
        );
        try {
            connection = factory.createConnection();
            connection.setClientID(clientId);
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            destination = session.createQueue(topicName);
            consumer = session.createConsumer(destination);
            connection.start();
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    public void setListener(MessageListener listener) throws JMSException {
        consumer.setMessageListener(listener);
    }

    public String receiveMessage() throws JMSException {
        TextMessage message = (TextMessage) consumer.receive();
        return message.getText();
    }

    public void close() {
        try {
            connection.close();
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    public Item receiveItem() throws JMSException {
        TextMessage message = (TextMessage) consumer.receive();
        Gson gson = new Gson();
        return gson.fromJson(message.getText(), Item.class);
    }

    public ItemRequest receiveItemRequest() throws JMSException {
        TextMessage message = (TextMessage) consumer.receive();
        Gson gson = new Gson();
        return gson.fromJson(message.getText(), ItemRequest.class);
    }

    public Item receiveItem(String s) throws JMSException {
        Message message;
        do {
            message = consumer.receive();
            session.createProducer(message.getJMSDestination())
                    .send(message);
        } while (!s.equals(message.getJMSCorrelationID()));

        message.acknowledge();
        TextMessage text = (TextMessage) message;
        Gson gson = new Gson();
        return gson.fromJson(text.getText(), Item.class);
    }
    public Destination getDestination() {
        return destination;
    }

    public Session getSession() {
        return session;
    }
}
