package ru.inno.jms.request;

public class ItemRequest {
    private int id;

    public ItemRequest(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "ItemRequest{" +
                "id=" + id +
                '}';
    }
}
